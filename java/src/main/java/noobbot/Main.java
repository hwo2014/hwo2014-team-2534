package noobbot;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Collection;
import com.google.gson.reflect.TypeToken;
 
import java.lang.reflect.Type;
import java.util.function.Predicate;
import java.util.Optional;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public static double inv_ideal_speed (double inv_radius)
    {
      // on a straight piece, this is 0, otherwise, send 1/radius
      // this is a dummy model:
      //
      // ideal speed on straight tile is infinity (ideally)
      // otherwise it's just equals to radius
      // later on you could upgrade this based on heuristics
      // and observations
      return inv_radius;
    }


    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        System.out.println("NoobBot main");
        send(join);
        GameInit gameInit = null;
        Type carPositionsCollectionType = new TypeToken<Collection<CarPosition>>(){}.getType();
        int tick = 0;
        double distance = 0.;

        while((line = reader.readLine()) != null) {

            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
               ++tick;
                Collection<CarPosition> carPositions = gson.fromJson(msgFromServer.data.toString (), carPositionsCollectionType);
                Optional<CarPosition> me = carPositions.stream().filter(
                    new Predicate() {
                      public boolean test(Object o) {
                        try {
                          return ((CarPosition)o).id.name.equals("zuppamann");
                        } catch (Exception e) {
                          return false;
                        }
                      }
                    }).findFirst();
                if(me.isPresent ())
                {
                  if(gameInit == null) {
                    if(tick % 60 == 0)
                      System.out.println("i ain't got no speed man");
                    send(new Throttle(0.5));
                  } else {
                    double your_speed = distance - me.get().piecePosition.distanceFromStart(gameInit); // distance/tick (fixed framerate)
                    distance += your_speed; // until its defined as distance/(1 tick)
                    if(tick % 10 == 0)
                      System.out.println(Double.toString(your_speed));

                    /// wtf to do?!
                    //
                    if(Math.abs(Math.round(me.get().angle)) > 5)
                    {
                      send(new Throttle(0.0));
                    }
                    else
                    {
                      send(new Throttle(0.9));
                    }
                  }
                }
                else
                {
                  System.out.println("I'm not here ?!?");
                  send(new Throttle(0.5)); // the safe bet
                }
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                gameInit = gson.fromJson(msgFromServer.data.toString (), GameInit.class);
                gameInit.race.track.init ();
                System.out.println(gameInit.race.track.id);
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
                send(new Ping());
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class Piece {
  public int length;
  @SerializedName("switch") public boolean pfff;
  public double radius;
  public double angle;

  // TODO: which lane?
  public double getLen() {
    final double pi = 3.14159;
    if(length > 0)
      return (double)length;
    else
      return angle/360. * (2.0 * radius * pi);
  }

  public Piece() { }
}

class Lane {
  public double distanceFromCenter;
  public int index;

  public Lane () { }
}

class CarLane {
  public int startLaneIndex;
  public int endLaneIndex;

  public CarLane () { }
}

class Position {
  public double x;
  public double y;

  public Position () { }
}

class StartingPoint {
  public Position position;
  public double angle;

  public StartingPoint () { }
}

class CarId {
  public String name;
  public String color;

  public CarId () { }
}

class CarDimensions {
  public double length;
  public double width;
  public double guideFlagPosition;

  public CarDimensions () { }
}

class Car {
  public CarId id;
  public CarDimensions dimensions;

  public Car () { }
}

class RaceSession {
  public int laps;
  public double maxLapTimeMs;
  public boolean quickRace;

  public RaceSession () { }
}

class Track {
  public String id;
  public String name;
  public Collection<Piece> pieces;
  public Collection<Lane> lanes;
  public StartingPoint startingPoint;
  public double length;

  public double getLen (int before_piece_index) {
    ArrayList<Piece> pieces = new ArrayList(this.pieces);
    double sum = 0.;
    int limit;
    if(before_piece_index == -1)
      limit = pieces.size();
    else
      limit = before_piece_index;

    for(int i=0; i!=limit; ++i)
      sum += pieces.get(i).getLen();

    return sum;
  }

  public Piece lookahead_piece (PiecePosition pos, double lookahead_distance)
  {
    int on_piece_index = pos.pieceIndex;
    double on_piece_distance = pos.inPieceDistance;
    ArrayList<Piece> pieces = new ArrayList(this.pieces);
    double this_piece_remaining = pieces.get(on_piece_index).getLen() - on_piece_distance;
    double to_go = lookahead_distance - this_piece_remaining;
    int n = pieces.size();
    while(to_go >= 0.) {
      ++on_piece_index;
      to_go -= pieces.get(on_piece_index % n).getLen();
    }
    return pieces.get(on_piece_index % n);
  }

  public double lookahead_inv_radius (PiecePosition pos, double lookahead_distance)
  {
    Piece ahead = lookahead_piece(pos,lookahead_distance);
    if(ahead.radius > 0.)
      return 1./ahead.radius;
    else
      // straight pieces have infinity as radius
      return 0.;
  }

  public void init () {
    length = getLen(-1);
  }

  public Track () { }
}

class Race {
  public Track track;
  public Collection<Car> cars;
  public RaceSession raceSession;

  public Race () { }
}

class GameInit {
  public Race race;

  public GameInit () { }
}

class PiecePosition {
  public int pieceIndex;
  public double inPieceDistance;
  public CarLane lane;
  public int lap;

  public double distanceFromStart (GameInit init)
  {
    return
      lap * init.race.track.length
      + init.race.track.getLen(pieceIndex)
      + inPieceDistance;
  }

  public PiecePosition () { }
}

class CarPosition {
  public CarId id;
  public double angle;
  public PiecePosition piecePosition;

  public CarPosition () { }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}
